﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class VR_TriggerController : MonoBehaviour
{
    public OVRInput.Button GetBut;
    public UnityEvent OnButDown;
    public UnityEvent OnButUp;
    public UnityEvent OnButTrigger;
    public UnityEvent OnButTriggerExit;
    bool isHandIn = false;
    // Start is called before the first frame update
    void Start()
    {
        
    }
    // Update is called once per frame
    //void Update()
    //{
        
    //}
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "HandR" || other.tag == "HandL" || other.tag == "HandTrigger")
        {
            isHandIn = true;
            if (OnButTrigger != null)
            {
                this.OnButTrigger.Invoke();
            }
        }
    }
    private void OnTriggerStay(Collider other)
    {
        if (OVRInput.GetDown(GetBut) && isHandIn == true)
        {
            if (OnButDown != null)
            {
                this.OnButDown.Invoke();
            }
        }
        if (OVRInput.GetUp(GetBut) && isHandIn == true)
        {
            if (OnButUp != null)
            {
                this.OnButUp.Invoke();
            }
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "HandR" || other.tag == "HandL" || other.tag == "HandTrigger")
        {
            isHandIn = false;
            if (OnButTriggerExit != null)
            {
                this.OnButTriggerExit.Invoke();
            }
        }
    }
}
