﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;


public class Box_Event : MonoBehaviour
{
    //星點事件
    //1.發亮
    //2.生成or彈出對應UI
    //2.收起UI
    //3.動畫表演
    Outline _outline;
    public Color _color;
    public Color OriColor;
    public Vector3 OriScale;
    public Vector3 TargetPosition;
    public ParticleSystem _particle;
    public GameObject _mesh;
    private void Start()
    {
        _outline = _mesh.GetComponent<Outline>();
        OriColor = _outline.OutlineColor;
    }
    public void BoxEventOn()
    {
        _mesh.transform.localScale = new Vector3((OriScale.x)*0.5f, (OriScale.y) * 0.5f,(OriScale.z) * 0.5f);
        //_outline.enabled = true;
        _outline.OutlineColor = _color;
        _particle.Play();
        _mesh.transform.DOKill();
        _mesh.transform.DOScale(OriScale, 0.8f).SetEase(Ease.OutElastic);
    }
    public void BoxEventExit()
    {
        //_outline.enabled = false;
        _outline.OutlineColor = OriColor;
    }
    public void BoxZoomIn()
    {
        this.gameObject.transform.DOKill();
        this.gameObject.transform.DOMove(TargetPosition, 2).SetEase(Ease.InOutBack);
        //print("88");
    }
}
