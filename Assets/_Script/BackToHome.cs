﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
/*2019.10.09更改
 * Input觸發方式更改
 * 
 */
public class BackToHome : MonoBehaviour {
    //public GameObject HandR, HandL;
    private bool isTrigger;
    public void SetTrigger(bool boo)//讓Input Manager控isTrigger的True/False
    {
        isTrigger = boo;
    }
    
    private void OnTriggerStay(Collider other)//讓這裡決定要做的事情，還有觸碰collider的偵聽
    {
        if(other.gameObject.tag =="HandL" ||other.gameObject.tag == "HandR")
        {
            if (isTrigger)
            {
                SceneManager.LoadScene(0);
            }
        }
    }
    /*
    private IEnumerator SetTriggerBack()
    {
        yield return new WaitForSeconds(1f);
        isTrigger = false;
    }
    */
}
