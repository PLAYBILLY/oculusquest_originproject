﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class StartScene_Manager : MonoBehaviour
{

    //製作星河點擊事件
    //1.生成星點
    //2.進場
    //3.紀錄星點狀態
    public static StartScene_Manager Manager;
    public static bool canAction;

    public GameObject[] _gameObjs;
    public int OpeningStar;
    public Image _image;
    private void Awake()
    {
        Manager = this;
    }
    void Start()
    {
        canAction = false;
        //EyeChangeScene();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void EyeChangeScene()
    {
        _image.DOColor(new Vector4(_image.color.r, _image.color.g, _image.color.b, 1), 1.5f).OnComplete(() => {
            _image.DOColor(new Vector4(_image.color.r, _image.color.g, _image.color.b, 0), 1.5f).SetDelay(2);
        });
    }
}
