﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
/*2019.10.8
 * 更改成配適Oculus版本
 * 
 * */
public class BallFloatAnimation : MonoBehaviour {
    public GameObject Hand_R, Hand_L;
    public float MaxFloatHeightY;
    public float FloatSpeed,RotateSpeed;
    public static bool isTouch;
    private bool isUp;
    private float InitialHeight;
    private bool isTrigger;
    public Text _text;
    // Use this for initialization
    void Start () {
        isUp = true;
        InitialHeight = gameObject.transform.localPosition.y;
    }
    private void Update()
    {
        
        if (Input.GetKeyDown(KeyCode.Keypad1))
        {
            StartCoroutine(NextScene(1));
        }
        if (Input.GetKeyDown(KeyCode.Keypad2))
        {
            StartCoroutine(NextScene(2));
        }
        if (Input.GetKeyDown(KeyCode.Keypad3))
        {
            StartCoroutine(NextScene(3));
        }
        if (Input.GetKeyDown(KeyCode.Keypad4))
        {
            StartCoroutine(NextScene(4));
        }
        if (gameObject.transform.localPosition.y > MaxFloatHeightY)
        {
            isUp = false;
        }
        if(gameObject.transform.localPosition.y < InitialHeight-0.01f)
        {
            isUp = true;
        }
        if (isUp)
        {
            transform.Translate(Vector3.up * Time.deltaTime * 0.1f* FloatSpeed);
        }
        else
        {
            transform.Translate(-Vector3.up * Time.deltaTime * 0.1f* FloatSpeed);
        }
        gameObject.transform.Rotate(new Vector3(0, RotateSpeed, 0));
    }
    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag == "HandL" || other.gameObject.tag == "HandR")//偵測是否為左右手
        {
            //_text.text = "hand in";
            if (isTrigger)//偵測是否按下Trigger
            {
                //_text.text = "hand Press";
                if (!isTouch)//只執行一次的防呆
                {
                    //_text.text = "function Start";
                    gameObject.transform.parent.GetComponent<Animator>().enabled = true;
                    switch (gameObject.name)
                    {
                        case "menu_ball_1":
                            StartCoroutine(NextScene(1));
                            break;
                        case "menu_ball_2":
                            StartCoroutine(NextScene(2));
                            break;
                        case "menu_ball_3":
                            StartCoroutine(NextScene(3));
                            break;
                        case "menu_ball_4":
                            StartCoroutine(NextScene(4));
                            break;
                    }
                    isTouch = true;
                }
            }
        }
    }

    //Trigger由InputManager處理觸發
    public void SetTrigger()
    {
        isTrigger = true;
    }
    //Trigger由InputManager處理觸發
    public void SetTriggerBack()
    {
        isTrigger = false;
    }
    IEnumerator NextScene(int SceneIndex)
    {
        yield return new WaitForSeconds(2.8f);
        isTouch = false;
        UnityEngine.SceneManagement.SceneManager.LoadScene(SceneIndex);
    }
}
    