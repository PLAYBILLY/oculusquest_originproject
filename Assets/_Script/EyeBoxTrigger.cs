﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class EyeBoxTrigger : MonoBehaviour
{
    public UnityEvent OnLooking;
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Eye" )
        {
                this.OnLooking.Invoke();
        }
    }
}
