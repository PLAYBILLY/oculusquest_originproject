﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RenderHeads;
using RenderHeads.Media.AVProVideo;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour {
    public float StartTime_Secs,EndTime_Secs;
    public GameObject AVProPlayer;
    private MediaPlayer mp;
	// Use this for initialization
	void Start () {
       mp = AVProPlayer.GetComponent<MediaPlayer>();
        if (StartTime_Secs <= 0)
            StartTime_Secs = 0;

        StartCoroutine(_Seeking());
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.Keypad0))
        {
            SceneManager.LoadScene(0);
        }
        if (EndTime_Secs <= 0)
            EndTime_Secs = mp.Info.GetDurationMs();

        if (mp.Control.GetCurrentTimeMs() >= (mp.Info.GetDurationMs()-(mp.Info.GetDurationMs()- EndTime_Secs*1000)) && mp.Control.GetCurrentTimeMs()>0)
        {
            //VideoEnd - GameEnd
            UnityEngine.SceneManagement.SceneManager.LoadScene(0);
        }
	}
    IEnumerator _Seeking()
    {
        yield return new WaitForSeconds(0.2f);
        mp.Control.Seek(StartTime_Secs * 1000);
    }
}
