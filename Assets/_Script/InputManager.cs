﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class InputManager : MonoBehaviour
{
    public VideoManager videoManager = null;
    public OVRInput.Button GetBut;
    public Text text;
    [SerializeField]
    public UnityEvent OnHandTriggerL;
    public UnityEvent OnHandTriggerR;
    public UnityEvent OnIndexTrigger;
    public UnityEvent DoneIndexTrigger;
    public UnityEvent OnIndexTriggerL;
    public UnityEvent OnIndexTriggerR;
    public UnityEvent OnOneTrigger;
    public UnityEvent OnBut;

    private void Update()
    {
        /*
        if (!videoManager.IsVideoReady)
        {
            return;
        }
        */
        OculusInput();
        KeyboardInput();
        
    }
    private void OculusInput()
    {
        if (OVRInput.GetDown(GetBut)) 
        {
            if (OnBut != null)
            {
                this.OnBut.Invoke();
            }
            //Debug.Log("1");
        }
        if (OVRInput.GetDown(OVRInput.Button.One, OVRInput.Controller.All))
        {
            if (OnOneTrigger != null)
            {
                this.OnOneTrigger.Invoke();
            }
            if (text.text != null)
            {
                text.text = "1";
            }
            //Debug.Log("1");
        }

        if(OVRInput.GetDown(OVRInput.Button.PrimaryHandTrigger, OVRInput.Controller.LTouch))
        {
            //text.text = "2";
            //Debug.Log("2");
            if (OnHandTriggerL != null)
            {
                this.OnHandTriggerL.Invoke();
            }
        }

        if (OVRInput.GetDown(OVRInput.Button.PrimaryHandTrigger, OVRInput.Controller.RTouch))
        {
            //text.text = "Play";
            //videoManager.PlayVideo();
            if (OnHandTriggerR != null)
            {
                this.OnHandTriggerR.Invoke();
            }
            
            //Debug.Log("3");
        }
        if (OVRInput.GetDown(OVRInput.Button.PrimaryIndexTrigger, OVRInput.Controller.All))
        {
            if (OnIndexTrigger != null)
            {
                this.OnIndexTrigger.Invoke();
            }
        }
        if (OVRInput.GetUp(OVRInput.Button.PrimaryIndexTrigger, OVRInput.Controller.All))
        {
            if (DoneIndexTrigger != null)
            {
                this.DoneIndexTrigger.Invoke();
            }
        }
        if (OVRInput.GetDown(OVRInput.Button.PrimaryIndexTrigger, OVRInput.Controller.LTouch))
        {
            if (OnIndexTriggerL != null)
            {
                this.OnIndexTriggerL.Invoke();
            }
            //text.text = "4";
            //Debug.Log("4");
        }

        if (OVRInput.GetDown(OVRInput.Button.PrimaryIndexTrigger, OVRInput.Controller.RTouch))
        {
            if (OnIndexTriggerR != null)
            {
                this.OnIndexTriggerR.Invoke();
            }
            //videoManager.PauseToggle();
            //text.text = "PauseToggle";
            //Debug.Log("5");
        }
    }

    private void KeyboardInput()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            //videoManager.PlayVideo();
            //text.text = "1";
        }

        if (Input.GetKeyDown(KeyCode.LeftArrow))
        {

        }

        if (Input.GetKeyDown(KeyCode.RightArrow))
        {

        }

        if (Input.GetKeyDown(KeyCode.DownArrow))
        {

        }

        if (Input.GetKeyDown(KeyCode.UpArrow))
        {

        }
    }
    /*
    public void EventMessage()
    {
        text.text += "EventOn";
    }
    */
}
