﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Video;
using UnityEngine.SceneManagement;

public class VideoManager : MonoBehaviour
{
    public List<VideoClip> videos = null;
    private bool isPaused = false;
    public bool IsPaused
    {
        get
        {
            return isPaused;
        }
        private set
        {
            isPaused = value;
            //Pause Event
        }
    }
    private bool isVideoReady = false;
    public bool IsVideoReady
    {
        get
        {
            return isVideoReady;
        }
        private set
        {
            isVideoReady = value;
            //Ready Event
        }
    }
    private int index = 0;
    private VideoPlayer videoPlayer = null;
    public bool UseFileVideo;
    public string Url;

    private void Awake()
    {
        videoPlayer = GetComponent<VideoPlayer>();
    }
    private void Start()
    {
        videoPlayer.loopPointReached += EndReached;
        if (Url != null)
        {
            videoPlayer.url = Url;
        }
        if (UseFileVideo)
        {
            videoPlayer.url = Application.persistentDataPath +"/"+ Url;
        }
        videoPlayer.Play();
    }

    void EndReached(VideoPlayer vp)
    {
        SceneManager.LoadScene(0);
    }

    public void PauseToggle()
    {
        IsPaused = !videoPlayer.isPaused;

        if (isPaused)
            videoPlayer.Pause();
        else
            videoPlayer.Play();
    }
    public void PlayVideo()
    {
        videoPlayer.Play();
    }
    public void PauseVideo()
    {
        videoPlayer.Pause();
    }



}
