﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;


public class ControllerTrigger : MonoBehaviour
{
    public OVRInput.Button GetBut;
    public UnityEvent OnButDown;
    public UnityEvent OnButUp;

    void Update()
    {
        if (OVRInput.GetDown(GetBut))
        {
            if (OnButDown != null)
            {
                this.OnButDown.Invoke();
            }
        }
        if (OVRInput.GetUp(GetBut))
        {
            if (OnButUp != null)
            {
                this.OnButUp.Invoke();
            }
        }
    }
}
