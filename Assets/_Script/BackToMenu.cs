﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
//using RenderHeads.Media.AVProVideo;
/*2019.10.09更改
 * 移除AVProVideo，以videoManager替代(-16、20、45、67)
 * 移除Animation_GO，未知的物件
 * Input觸發方式更改
 */

public class BackToMenu : MonoBehaviour {
    public VideoManager videoManager;
    private Animator ReturnBall_Animator, Animator_GO;
    public GameObject Mask, ReturnBall_Animator_GO,/* Animation_GO,*/Sand;
    private bool isOpen = false;
    //public GameObject AVProPlayer;
    //private MediaPlayer mp;

    private void Start()
    {
        //mp = AVProPlayer.GetComponent<MediaPlayer>();

        //未知的物件
        /*
        if (Animation_GO != null)
        {
            Animator_GO = Animation_GO.GetComponent<Animator>();
        }
        */
        ReturnBall_Animator = ReturnBall_Animator_GO.GetComponent<Animator>();
        //ReturnBall_Animator_GO.SetActive(false);
        Mask.SetActive(false);
    }
    public void m_BackToMenu()//當Controller按到Pad時觸發
    {
        //SceneManager.LoadScene(0);
        if (!isOpen)
        {
            StartCoroutine(_Animation_return_Open());
            Mask.SetActive(true);
        }
        else
        {
            StartCoroutine(_Animation_return_Close());
            Mask.SetActive(false);
        }
    }
    IEnumerator _Animation_return_Open()//影片暫停、啟動暫停畫面
    {
        //mp.Control.Pause();
        videoManager.PauseVideo();
        if (Sand != null)
            Sand.SetActive(false);
        /*
        if (Animation_GO != null)
            Animator_GO.enabled = false;
        */

        if (ReturnBall_Animator.enabled)
        {
            ReturnBall_Animator.Play("return_ball");
            ReturnBall_Animator.SetBool("isChoose", false);
        }
        else
        {
            ReturnBall_Animator.enabled = true;
            ReturnBall_Animator_GO.SetActive(true);
            ReturnBall_Animator.SetBool("isChoose", false);
        }
        yield return new WaitForSeconds(0.5f);
        isOpen = true;
    }
    IEnumerator _Animation_return_Close()//關閉暫停畫面，啟動影片
    {

        if(Sand!=null)
            Sand.SetActive(true);
        /*
        if (Animation_GO != null)
            Animator_GO.enabled = true;
        */
        ReturnBall_Animator.SetBool("isChoose", true);
        yield return new WaitForSeconds(0.5f);
        //mp.Control.Play();
        videoManager.PlayVideo();
        isOpen = false;
    }
}
