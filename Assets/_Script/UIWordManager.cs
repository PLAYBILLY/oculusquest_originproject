﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class UIWordManager : MonoBehaviour
{
    public Text _text;
    public string[] _str;
    public int nowNumber;
    public float showSpeed = 1;

    public void WordFunction(int i = 0)
    {
        //if (nowNumber >= _str.Length)
        //{
        //    print("NoMoreWordCanShow");
        //    return;
        //}
        if (i != 0)
        {
            nowNumber = i;
        }
        float showSecond = (_str[nowNumber].Length) / 3 / showSpeed;
        _text.text = " ";
        _text.DOKill();
        _text.DOText(_str[nowNumber], showSecond);
        if (i == 0 && nowNumber < _str.Length-1)
        {
            nowNumber++;
        }
    }
    
}
